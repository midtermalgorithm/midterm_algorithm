/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.midterm_algorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.nio.file.Files.list;
import static java.rmi.Naming.list;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Midterm_Algorithm2 {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Scanner kb = new Scanner(System.in);
        String a = kb.next();
        File inputfile = new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_1.txt");
        Scanner type = new Scanner(checkfile(a, inputfile));
        ArrayList<String> people = new ArrayList<>();
        while (type.hasNext()) {
            people.add(type.next());
        }

        startGame(people, 5);
        System.out.println(people+" is The last people");
    }

     static File checkfile(String name, File type) throws IOException {
        switch (name) {
            case ("1.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_1.txt"));
                break;
            case ("2.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_2.txt"));
                break;
            case ("3.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_3.txt"));
                break;
            case ("4.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_4.txt"));
                break;
            case ("5.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_5.txt"));
                break;
            case ("6.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_6.txt"));
                break;
            case ("7.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_7.txt"));
                break;
            case ("8.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_8.txt"));
                break;
            case ("9.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_9.txt"));
                break;
            case ("10.in"):
                type = (new File("C:\\Users\\User\\Documents\\NetBeansProjects\\Midterm_Algorithm\\Midterm 2.1\\input2_10.txt"));
                break;

        }
        return type;
    }

    static String startGame(ArrayList<String> kid, int k) {
        int index = 0;
        if (kid.size() == 1) {

            return kid.get(0);
        }
        index = ((index + k) % kid.size());
        kid.remove(index);
        startGame(kid, k);
        return startGame(kid, k);
    }

}
